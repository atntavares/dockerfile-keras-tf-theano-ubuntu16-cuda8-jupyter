# keras-tf-theano-ubuntu16-cuda8-jupyter

Keras with both Theano and TensorFlow backends on Ubuntu 16.04. Cuda 8.0 and Jupyter pre-installed.

## Run with port forwarding:

```


docker run -d -p 8888:8888 atavares/keras-tf-theano-ubuntu16-cuda8-jupyter
```


## Run with a [specific backend](https://keras.io/backend/): 


```


docker run -d -p 8888:8888 -e KERAS_BACKEND=<backend> atavares/keras-tf-theano-ubuntu16-cuda8-jupyter
```


Where the <backend> is either "theano" or "tensorflow" (no commas)

## Run from local drive for persistent storage:


```


docker run -d -p 8888:8888 -v /notebook:/notebook atavares/keras-tf-theano-ubuntu16-cuda8-jupyter
```


## Accessing the jupyter notebook

Just browse [localhost:8888](localhost:8888)